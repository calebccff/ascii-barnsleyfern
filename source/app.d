 import graphics;
import std.random;
//import vector;
import std.conv;
import std.stdio;

float posX = 0, posY = 0;
int[2][3][2] corners;

void main(){
  new Game().start();
}

class Game : Canvas {

  override void setup(){
    //First triange
    corners[0][0][0] = width/2;
    corners[0][0][1] = 0;

    corners[0][1][0] = 0;
    corners[0][1][1] = height;

    corners[0][2][0] = width;
    corners[0][2][1] = height;
    //Second triangle

    corners[1][0][0] = 100;
    corners[1][0][1] = 5;

    corners[1][1][0] = 50;
    corners[1][1][1] = 43;

    corners[1][2][0] = 80;
    corners[1][2][1] = 40;

    for(int t = 0; t < corners.length; t++){
      for(int i = 0; i < corners[t].length; i++){
        point(corners[t][i][0], corners[t][i][1], "+");
      }
    }
    writef("\033[32m");
  }

  override void draw(){
    int chance = uniform(0, 100);
    if(chance < 1){
      posX = 0;
      posY = posY*0.16;
    }else if(chance <= 85){
      posX = 0.85*posX+0.04*posY;
      posY = -0.04*posX+0.85*posY+1.6;
    }else if(chance < 93){
      posX = 0.2*posX-0.26*posY;
      posY = 0.23*posX+0.22*posY+1.6;
    }else{
      posX = -0.15*posX+0.28*posY;
      posY = 0.26*posX+0.24*posY+0.44;
    }
    point(to!int(posY*15+6), to!int(height*0.4+1+posX*10), to!string(cast(char)uniform(48, 127))); //Picks a random visible ASCII character
    graphics.text(to!string(width)~" : "~to!string(height), 0, 0);
    graphics.text(to!string(posX)~" : "~to!string(height-posY)~"   ", 1, 2);
    
  }

  int lerp(float s, float e, float p){
    return to!int(s+p*(e-s));
  }

  int constrain(int x, int min, int max){
    return x<min?min:x>max?max:x;
  }
}
